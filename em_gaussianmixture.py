# -*- coding: utf-8 -*-
u"""
混合ガウス分布をEMアルゴリズムで.

(参考) はじめてのパターン認識
"""
import numpy as np
from scipy import linalg as splinalg


def multinormal_pdf(x, mu, cov):
	u"""多次元正規分布の密度関数."""
	n, d = x.shape
	chol = get_chol(cov)

	pdfs = np.array([
		np.exp(-0.5 * np.dot((x[i, :] - mu), splinalg.cho_solve((chol, False), (x[i, :] - mu))))
		/ np.sqrt((2 * np.pi)**d * chol_det(chol))
		for i in xrange(n)
	])
	return pdfs


def log_pdf(x, mu, cov):
	u"""多次元正規分布のlogスケールの密度関数."""
	n, d = x.shape
	chol = get_chol(cov)

	log_pdfs = np.array([
		-0.5 * (
			np.dot((x[i, :] - mu), splinalg.cho_solve((chol, False), (x[i, :] - mu)))
			+ d * np.log(2 * np.pi) + chol_logdet(chol)
		)
		for i in xrange(n)
	])
	return log_pdfs


def get_chol(cov):
	u"""正定値を保ちながらコレスキ分解する."""
	c = cov
	d, _ = cov.shape
	while 1:
		try:
			chol = splinalg.cholesky(c, check_finite=False)
			break
		except np.linalg.LinAlgError:
			print "modified"
			c += np.diag([1e-6] * d)
	return chol


def chol_det(chol):
	u"""コレスキ分解した行列を使った行列式計算."""
	return np.product(np.diag(chol))**2


def chol_logdet(chol):
	u"""コレスキ分解した行列を使った行列式計算."""
	return 2 * np.sum(np.log(np.diag(chol)))


class EMGaussianMixture(object):
	u"""EMで混合ガウスクラスタリング."""

	def __init__(self, data, k, fit_ratio=True, diag=False):
		u"""初期化."""
		self.data_ = data
		self.k_ = k                  # クラスタサイズ
		self.post_ = 0           # 隠れ変数zの事後確率

		n, d = self.data_.shape
		self.mu_ = [np.random.random(size=d) for i in xrange(k)]
		self.cov_ = [np.identity(d) for i in xrange(k)]
		self.ratio_ = np.ones(k) / k

		# オプション
		self.ratio_on = fit_ratio       # 混合比固定
		self.diag_ = diag               # 共分散を考えない

	def fit(self):
		u"""最適化."""
		ite = 1
		preq = 0
		while 1:
			print "========================================"
			print "Iteration {}".format(ite)
			print "========================================"
			self.m_step()
			self.e_step()
			q = self.qfunc()
			print "qvalue   : {}".format(q)
			if np.isnan(q):
				print "qvalue is nan"
				break

			if ite == 1:
				preq = q
			else:
				print q - preq
				if q - preq < 1e-6:
					break
				else:
					preq = q
			ite += 1

	def get_label(self):
		u"""ラベル付けする."""
		n, d = self.data_.shape
		return np.argmax(self.post_, axis=0)

	def m_step(self):
		u"""
		Mステップ.

		隠れ変数zの期待値 = 事後確率を計算する.
		"""
		kpdf = np.array([
			multinormal_pdf(self.data_, self.mu_[i], self.cov_[i])
			for i in xrange(self.k_)
		])                             # 各クラスタの確率密度 k x n
		self.post_ = self.ratio_[:, np.newaxis] * kpdf / np.dot(self.ratio_, kpdf)
		# z_ikの事後確率 k x n

	def e_step(self):
		u"""
		Eステップ.

		Q関数をmu, Sigma, 混合比について最大化する.
		"""
		post = self.post_
		mu = self.mu_              # 古いmuを保存
		X = self.data_
		n, _ = X.shape

		# 平均の更新
		nk = np.sum(post, axis=1)
		new_mu = np.dot(post, X) / nk[:, np.newaxis]
		self.mu_ = [m for m in new_mu]

		# 共分散の更新
		if self.diag_:
			self.cov_ = [
				np.diag(np.sum((X - mu[k])**2 * post[k, :][:, np.newaxis], axis=0) / nk[k])
				for k in xrange(self.k_)
			]

		else:
			self.cov_ = [
				np.dot((X - mu[k]).T, np.dot(np.diag(post[k, :]), (X - mu[k]))) / nk[k]
				for k in xrange(self.k_)
			]

		if self.ratio_on:
			# 混合比の更新
			self.ratio_ = nk / n

	def qfunc(self):
		u"""Q関数の計算."""
		n, d = self.data_.shape
		X = self.data_
		mu = self.mu_
		cov = self.cov_

		q = np.sum([
			np.dot(self.post_[k, :], log_pdf(X, mu[k], cov[k]))
			for k in xrange(self.k_)
		]) + np.sum(np.dot(self.post_.T, np.log(self.ratio_)))
		return q

















































